﻿// Copyright (c) WithLithum & contributors 2023-2024. All rights reserved.
// Licensed under the GNU Lesser General Public License, either version 3 or
// (at your opinion) any later version.

namespace MineJason.Data.Selectors;

using System.Globalization;

/// <summary>
/// Represents an exact match scoreboard value.
/// </summary>
/// <param name="objective">The objective.</param>
/// <param name="value">The value.</param>
public struct ScoreboardExactMatch(string objective, int value) : IScoreboardRange, IEquatable<ScoreboardExactMatch>
{
    /// <summary>
    /// Gets or sets the value of this instance.
    /// </summary>
    public int Value { get; set; } = value;

    /// <inheritdoc />
    public readonly bool Equals(ScoreboardExactMatch other)
    {
        return Value == other.Value;
    }

    /// <inheritdoc />
    public override readonly bool Equals(object? obj)
    {
        return obj is ScoreboardExactMatch other && Equals(other);
    }

    /// <inheritdoc />
    public override readonly int GetHashCode()
    {
        return Value;
    }

    /// <summary>
    /// Gets or sets the objective to match.
    /// </summary>
    public string Objective { get; } = objective;

    /// <inheritdoc />
    public readonly string GetString()
    {
        return Value.ToString(CultureInfo.InvariantCulture);
    }

    /// <summary>
    /// Determines whether the instance to the left is equivalent to the instance to the right. 
    /// </summary>
    /// <param name="left">The instance to the left.</param>
    /// <param name="right">The instance to the right.</param>
    /// <returns><see langword="true"/> if the instances are equivalent; otherwise, <see langword="false"/>.</returns>
    public static bool operator ==(ScoreboardExactMatch left, ScoreboardExactMatch right)
    {
        return left.Equals(right);
    }

    /// <summary>
    /// Determines whether the instance to the left is different to the instance to the right. 
    /// </summary>
    /// <param name="left">The instance to the left.</param>
    /// <param name="right">The instance to the right.</param>
    /// <returns><see langword="true"/> if the instances are different; otherwise, <see langword="false"/>.</returns>
    public static bool operator !=(ScoreboardExactMatch left, ScoreboardExactMatch right)
    {
        return !(left == right);
    }
}