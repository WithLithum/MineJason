﻿// Copyright (c) WithLithum & contributors 2023-2024. All rights reserved.
// Licensed under the GNU Lesser General Public License, either version 3 or 
// (at your opinion) any later version.

namespace MineJason.Data.Selectors;

using System;
using System.Collections.ObjectModel;

/// <summary>
/// A collection of <see cref="EntityNameMatch"/>.
/// </summary>
public class EntityNameMatchCollection : Collection<EntityNameMatch>
{
    internal void WriteToBuilder(EntitySelectorArgumentBuilder builder)
    {
        foreach (var item in this)
        {
            builder.WritePair("name", item.Value ? item.Name : $"!{item.Name}");
        }
    }
}
